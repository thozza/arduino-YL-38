# Arduino sketch for testing YL-38 analog moisture sensor
This repository includes the firmware for testing YL-38 analog moisture sensor. The
firmware is designed for Arduino Uno board. For
building the firmware, the use of [PlatformIO](http://platformio.org)
tool is assumed.

## Getting the source
Used libraries are included in this repository.

## Building the firmware
Building is easy as running a single command:
```
platformio run
```

## Uploading the firmware
Uploading is again super easy by running:
```
platformio run --target upload
```

## Connecting to the serial port
Just run:
```
platformio serialports monitor
```
