/*
 * Copyright (c) 2016, Tomas Hozza <thozza [at] gmail [dot] com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <Arduino.h>
#include "yl38.h"

#define YL38_SENSOR_READINGS 10

YL38Sensor::YL38Sensor(int analog_pin, int vcc_pin) :
    analog_pin(analog_pin),
    vcc_pin(vcc_pin)
{
}

int YL38Sensor::read_value() {
    int i = 0;
    int value = 0;

    poweron_sensor();

    for (int i = 0; i < YL38_SENSOR_READINGS; i++) {
        value += analogRead(this->analog_pin);
    }

    shutdown_sensor();

    return 1023 - (value / YL38_SENSOR_READINGS);
}

void YL38Sensor::begin() {
    poweron_sensor();
    // initialize the analog PIN
    pinMode(this->analog_pin, INPUT);
    shutdown_sensor();
}

void YL38Sensor::poweron_sensor() {
    // power on the sensor if the Vcc PIN is set
    if (this->vcc_pin != -1) {
        pinMode(this->vcc_pin, OUTPUT);
        digitalWrite(this->vcc_pin, HIGH);
        delay(500);
    }
}

void YL38Sensor::shutdown_sensor() {
    if (this->vcc_pin != -1) {
        digitalWrite(this->vcc_pin, LOW);
    }
}
